package com.amyandguys.smartcar.api

import com.amyandguys.smartcar.api.model.VehicleDataResponse
import retrofit2.Call
import retrofit2.http.GET

interface IVehicleDataApi {
    @GET("vehicle_data.json")
    fun getVehicleData(): Call<List<VehicleDataResponse>>
}