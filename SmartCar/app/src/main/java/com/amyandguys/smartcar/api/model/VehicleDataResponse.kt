package com.amyandguys.smartcar.api.model

import com.google.gson.annotations.SerializedName

data class VehicleDataResponse(
    @SerializedName("vin") val vehicleId: String,
    @SerializedName("odometer") val odometer: Float,
    @SerializedName("latitude") val latitude: Double,
    @SerializedName("longitude") val longitude: Double,
    @SerializedName("make") val make: String,
    @SerializedName("model") val model: String,
    @SerializedName("year") val year: Int
)