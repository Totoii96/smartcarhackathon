package com.amyandguys.smartcar.main

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.amyandguys.smartcar.R
import com.amyandguys.smartcar.gamemap.GameMapActivity
import com.amyandguys.smartcar.gameprompt.GamePromptActivity
import com.amyandguys.smartcar.player_stats.PlayerStatsActivity
import com.smartcar.sdk.SmartcarAuth
import com.smartcar.sdk.SmartcarCallback
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import okhttp3.Request
import javax.inject.Inject
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {
    private val REQUEST_CODE_PERMISSIONS = 2389

    @Inject lateinit var viewModel: IMainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AndroidInjection.inject(this)

        setOnClickerListeners()
    }

    private fun setOnClickerListeners() {
        test_auth.setOnClickListener {
            authenticate(true)
        }

        real_auth.setOnClickListener {
            authenticate(false)
        }

        show_map.setOnClickListener {
            startActivity(GameMapActivity.newIntent(this))
        }

        player_stats.setOnClickListener {
            startActivity(PlayerStatsActivity.newIntent(this))
        }

        game_prompt_button.setOnClickListener {
            startActivity(GamePromptActivity.newIntent(this))
        }
    }

    override fun onResume() {
        super.onResume()
        val requiredPermissions = packageManager.getPackageInfo(this.packageName, PackageManager.GET_PERMISSIONS).requestedPermissions
        val requestPermissions = requiredPermissions.filter { permission ->
            ContextCompat.checkSelfPermission(
                this,
                permission
            ) != PackageManager.PERMISSION_GRANTED
        }
        if (!requestPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                requestPermissions.toTypedArray(),
                REQUEST_CODE_PERMISSIONS
            )
        }
    }

    fun authenticate(developmentMode: Boolean = true) {
        val clientId = getString(R.string.client_id)
        val redirectUri = "sc$clientId://exchange"
        val scope = arrayOf(
            "read_vehicle_info",
            "read_odometer",
            "control_security",
            "control_security:unlock",
            "control_security:lock",
            "read_location"
        )

        SmartcarAuth(
            clientId,
            redirectUri,
            scope,
            developmentMode,
            SmartcarCallback { smartcarResponse ->
                val client = OkHttpClient()

                thread {
                    // send request to exchange the auth code for the access token
                    val exchangeRequest = Request.Builder()
                        .url(getString(R.string.app_server) + "/exchange?code=" + smartcarResponse.code)
                        .build()

                    try {
                        client.newCall(exchangeRequest).execute()
                    } catch (e: Exception) {
                        Log.i("MainActivity2", "UH OH, SOMETHING WENT WRONG")
                    }

                }
            }
        ).apply {
            launchAuthFlow(applicationContext)
        }
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent =
            Intent(
                context,
                MainActivity::class.java
            ).apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK }
    }
}
