package com.amyandguys.smartcar.injection

import com.amyandguys.smartcar.gamemap.GameMapActivity
import com.amyandguys.smartcar.gameprompt.GamePromptActivity
import com.amyandguys.smartcar.main.MainActivity
import com.amyandguys.smartcar.player_stats.PlayerStatsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    internal abstract fun contributesMainActivitiy(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributesGameMapActivity(): GameMapActivity

    @ContributesAndroidInjector
    internal abstract fun contributesGamePromptActivity(): GamePromptActivity

    @ContributesAndroidInjector
    internal abstract fun contributesPlayerStatsActivity(): PlayerStatsActivity
}