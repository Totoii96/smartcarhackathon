package com.amyandguys.smartcar.gamemap

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.amyandguys.smartcar.R
import com.amyandguys.smartcar.main.MainActivity
import com.amyandguys.smartcar.player_stats.PlayerStatsActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.smartcar.sdk.SmartcarAuth
import com.smartcar.sdk.SmartcarCallback
import dagger.android.AndroidInjection
import okhttp3.OkHttpClient
import okhttp3.Request
import javax.inject.Inject
import kotlin.concurrent.thread

class GameMapActivity : AppCompatActivity(), OnMapReadyCallback {

    private val REQUEST_CODE_PERMISSIONS = 2389

    @Inject lateinit var viewModel: GameMapViewModel

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)

        setContentView(R.layout.activity_game_map)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
        requestPermissions()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.maps_option_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.add_cars_item -> authenticate()
            R.id.stats_item -> startActivity(PlayerStatsActivity.newIntent(this))
            R.id.debug_item -> startActivity(MainActivity.newIntent(this))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        viewModel.onMapReady(googleMap)
    }

    private fun authenticate() {
        val clientId = getString(R.string.client_id)
        val redirectUri = "sc$clientId://exchange"
        val scope = arrayOf(
            "read_vehicle_info",
            "read_odometer",
            "control_security",
            "control_security:unlock",
            "control_security:lock",
            "read_location"
        )

        SmartcarAuth(
            clientId,
            redirectUri,
            scope,
            true,
            SmartcarCallback { smartcarResponse ->
                val client = OkHttpClient()

                thread {
                    // send request to exchange the auth code for the access token
                    val exchangeRequest = Request.Builder()
                        .url(getString(R.string.app_server) + "/exchange?code=" + smartcarResponse.code)
                        .build()

                    try {
                        client.newCall(exchangeRequest).execute()
                    } catch (e: Exception) {
                        Log.i("MainActivity2", "UH OH, SOMETHING WENT WRONG")
                    }

                    startActivity(GameMapActivity.newIntent(this))
                }
            }
        ).apply {
            launchAuthFlow(applicationContext)
        }
    }

    private fun requestPermissions() {
        val requiredPermissions =
            packageManager.getPackageInfo(this.packageName, PackageManager.GET_PERMISSIONS)
                .requestedPermissions
        val requestPermissions = requiredPermissions.filter { permission ->
            ContextCompat.checkSelfPermission(
                this,
                permission
            ) != PackageManager.PERMISSION_GRANTED
        }
        if (!requestPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                requestPermissions.toTypedArray(),
                REQUEST_CODE_PERMISSIONS
            )
        }
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent =
            Intent(
                context,
                GameMapActivity::class.java
            ).apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK }
    }
}
