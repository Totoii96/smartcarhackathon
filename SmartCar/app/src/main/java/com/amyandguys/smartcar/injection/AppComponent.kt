package com.amyandguys.smartcar.injection

import android.app.Application
import com.amyandguys.smartcar.SmartCar
import com.amyandguys.smartcar.injection.modules.RestApiModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityModule::class,
        RestApiModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(smartCar: SmartCar)
}