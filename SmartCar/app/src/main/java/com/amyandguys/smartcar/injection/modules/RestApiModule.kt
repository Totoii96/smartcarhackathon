package com.amyandguys.smartcar.injection.modules

import com.amyandguys.smartcar.api.IVehicleDataApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
internal class RestApiModule {
    @Provides
    fun providesGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()

    @Singleton
    @Provides
    fun providesOkHttpClient(): OkHttpClient =
        OkHttpClient().newBuilder().build()

    @Singleton
    @Provides
    fun providesIVehicleDataApi(okHttpClient: OkHttpClient): IVehicleDataApi {
        return Retrofit
            .Builder()
            .baseUrl("https://cardex.garfiec.com/")
            .addConverterFactory(providesGsonConverterFactory())
            .client(okHttpClient)
            .build()
            .create(IVehicleDataApi::class.java)
    }
}