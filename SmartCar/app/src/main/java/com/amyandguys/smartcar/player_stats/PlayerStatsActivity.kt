package com.amyandguys.smartcar.player_stats

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amyandguys.smartcar.R
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_player_stats.*
import kotlinx.android.synthetic.main.points_card.view.*
import javax.inject.Inject
import kotlin.math.min

class PlayerStatsActivity : AppCompatActivity() {
    @Inject lateinit var playerStats: PlayerStatsManager

    private lateinit var expAdapter: ExpAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)
        setContentView(R.layout.activity_player_stats)

        name_textview.text = playerStats.name
        level_textview.text = "Level ${playerStats.level}"
        exp_bar.progress = min(1000, playerStats.exp)
        exp_text.text = "${playerStats.exp} / 1000 XP"

        expAdapter = ExpAdapter(this)
        expAdapter.setData(playerStats.pointsHistory)

        points_history.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@PlayerStatsActivity)
            itemAnimator = DefaultItemAnimator()
            adapter = expAdapter
        }
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent =
            Intent(
                context,
                PlayerStatsActivity::class.java
            )
    }
}

class ExpAdapter(
    private val context: Context
) :
    RecyclerView.Adapter<ExpAdapter.ExpHolder>() {
    private var dataset: ArrayList<Int> = ArrayList()

    class ExpHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpHolder {
        val cardView = LayoutInflater.from(context).inflate(R.layout.points_card, parent, false)
        return ExpHolder(cardView)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(holder: ExpHolder, position: Int) {
        holder.itemView.apply {
            card_points.text = dataset[position].toString()
        }
    }

    fun setData(newData: ArrayList<Int>) {
        dataset = newData
        notifyDataSetChanged()
    }

}