package com.amyandguys.smartcar.injection

import com.amyandguys.smartcar.api.IVehicleDataApi
import com.amyandguys.smartcar.gamemap.GameMapViewModel
import com.amyandguys.smartcar.main.IMainViewModel
import com.amyandguys.smartcar.main.MainViewModel
import com.amyandguys.smartcar.player_stats.PlayerStatsManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class AppModule {
    @Singleton
    @Provides
    fun providesMainViewModel(): IMainViewModel = MainViewModel()

    @Singleton
    @Provides
    fun providesGameMapViewModel(vehicleDataApi: IVehicleDataApi): GameMapViewModel =
        GameMapViewModel(vehicleDataApi)

    @Singleton
    @Provides
    fun providesPlayerStatsManager(): PlayerStatsManager = PlayerStatsManager()
}