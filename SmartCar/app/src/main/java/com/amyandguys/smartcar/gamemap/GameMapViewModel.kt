package com.amyandguys.smartcar.gamemap

import android.os.Handler
import android.widget.Toast
import com.amyandguys.smartcar.R
import com.amyandguys.smartcar.SmartCar
import com.amyandguys.smartcar.api.IVehicleDataApi
import com.amyandguys.smartcar.api.model.VehicleDataResponse
import com.amyandguys.smartcar.gameprompt.GamePromptActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GameMapViewModel(
    private val vehicleData: IVehicleDataApi
) {
    private var googleMap: GoogleMap? = null
    private val mapMarkers: HashMap<Marker, VehicleDataResponse> = HashMap()

    fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        googleMap.isMyLocationEnabled = true
        googleMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(42.054766, -87.679863),
                10f
            )
        )
        googleMap.setOnMarkerClickListener { marker ->
            Toast.makeText(SmartCar.INSTANCE, "Clicked a car!", Toast.LENGTH_LONG).show()
            Handler().postDelayed(
                {
                    mapMarkers[marker]?.let { vehicle ->
                        SmartCar.INSTANCE.let {
                            it.startActivity(
                                GamePromptActivity.newIntent(
                                    context = it,
                                    vehicleId = vehicle.vehicleId,
                                    odometer = vehicle.odometer,
                                    latitude = vehicle.latitude,
                                    longitude = vehicle.longitude,
                                    make = vehicle.make,
                                    model = vehicle.model,
                                    year = vehicle.year
                                )
                            )
                        }
                    }
                }, 150 // For the animations
            )
            true
        }
        loadData()
    }

    fun onResume() {
        loadData()
    }

    private fun loadData() {
        vehicleData.getVehicleData()
            .enqueue(object : Callback<List<VehicleDataResponse>> {
                override fun onFailure(call: Call<List<VehicleDataResponse>>, t: Throwable) {}

                override fun onResponse(
                    call: Call<List<VehicleDataResponse>>,
                    response: Response<List<VehicleDataResponse>>
                ) {
                    response.body()?.let { vehicles -> addVehicles(vehicles) }
                }

            })
    }

    private fun addVehicles(vehicles: List<VehicleDataResponse>) {
        vehicles.forEach { vehicle ->

            googleMap?.apply {
                val googleMapMarker = addMarker(
                    MarkerOptions()
                        .position(LatLng(vehicle.latitude, vehicle.longitude))
                        .icon(
                            BitmapDescriptorFactory.fromResource(
                                R.drawable.car_icon
                            )
                        )
                )

                mapMarkers[googleMapMarker] = vehicle
            }

        }
    }

}