package com.amyandguys.smartcar.gameprompt

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.amyandguys.smartcar.R
import com.amyandguys.smartcar.api.model.VehicleDataResponse
import com.amyandguys.smartcar.gamemap.GameMapActivity
import com.amyandguys.smartcar.player_stats.PlayerStatsManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_game_prompt.*
import javax.inject.Inject

class GamePromptActivity : AppCompatActivity() {
    @Inject lateinit var playerStats: PlayerStatsManager
    var vehicleData: VehicleDataResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)

        setContentView(R.layout.activity_game_prompt)

        intent.extras?.let {
            vehicleData = VehicleDataResponse(
                vehicleId = it.getString("vehicleId"),
                odometer = it.getFloat("odometer"),
                latitude = it.getDouble("latitude"),
                longitude = it.getDouble("longitude"),
                make = it.getString("make"),
                model = it.getString("model"),
                year = it.getInt("year")
            )
        }

        submit_button.setOnClickListener {
            playerStats.exp += 34
            playerStats.pointsHistory.add(34)
            startActivity(GameMapActivity.newIntent(this))
        }

    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent =
            Intent(
                context,
                GamePromptActivity::class.java
            )

        fun newIntent(
            context: Context,
            vehicleId: String,
            odometer: Float,
            latitude: Double,
            longitude: Double,
            make: String,
            model: String,
            year: Int
        ): Intent =
            Intent(
                context,
                GamePromptActivity::class.java
            ).apply {
                putExtra("vehicleId", vehicleId)
                putExtra("odometer", odometer)
                putExtra("latitude", latitude)
                putExtra("longitude", longitude)
                putExtra("make", make)
                putExtra("model", model)
                putExtra("year", year)
            }
    }
}
