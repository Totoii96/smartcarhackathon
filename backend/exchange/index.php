<?php
	// Get auth code
	$authCode = $_GET['code'];
	
	// Now get access code
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://auth.smartcar.com/oauth/token",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=" . $authCode . "&redirect_uri=sc11124ee9-35b3-442a-9b3a-88fe075d1d74%3A%2F%2Fexchange",
	  CURLOPT_HTTPHEADER => array(
		"Authorization: Basic MTExMjRlZTktMzViMy00NDJhLTliM2EtODhmZTA3NWQxZDc0OjAwNWEzNzQ2LWJkNzAtNDA2OS1iNGQwLTQ5OTIyZDU0ZjQyYw=",
		"Cache-Control: no-cache",
		"Content-Type: application/x-www-form-urlencoded",
		"Postman-Token: bbf11dcc-20d3-4280-9a32-1c72aafac643"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
		echo $response;
		$file = "../access_code.txt";
		$arrayResponse = json_decode($response, true);

		$current = file_get_contents($file);
		$current .= $arrayResponse["access_token"] . "," . $arrayResponse["refresh_token"] . "\n";
		file_put_contents($file, $current);
	}

?>
