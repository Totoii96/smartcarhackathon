<pre>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$vehicles = array();

function getLocation($bearer, $vId) {
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.smartcar.com/v1.0/vehicles/" . $vId . "/location",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer " . $bearer,
		"Cache-Control: no-cache",
		"Postman-Token: 7b80b73a-1ef9-45da-bf2c-e805037a8c11"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  //echo $response;
	  $responseArray = json_decode($response, true);
	  return $responseArray;
	}
}

function getOdometer($bearer, $vId) {
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.smartcar.com/v1.0/vehicles/" . $vId . "/odometer",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer " . $bearer,
		"Cache-Control: no-cache",
		"Postman-Token: 7b80b73a-1ef9-45da-bf2c-e805037a8c11"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  //echo $response;
	  $responseArray = json_decode($response, true);
	  return $responseArray["distance"];
	}
}


function getVehicleInfo($bearer, $vId) {
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.smartcar.com/v1.0/vehicles/" . $vId,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer " . $bearer,
		"Cache-Control: no-cache",
		"Postman-Token: 7b80b73a-1ef9-45da-bf2c-e805037a8c11"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  //echo $response;
	  $responseArray = json_decode($response, true);
	  return $responseArray;
	}
}

function getVehicleIds($bearer) {
	global $vehicles;
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.smartcar.com/v1.0/vehicles",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer " . $bearer,
		"Cache-Control: no-cache",
		"Postman-Token: 7b80b73a-1ef9-45da-bf2c-e805037a8c11"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  //echo $response;
	  $responseArray = json_decode($response, true);
	  foreach($responseArray["vehicles"] as $vId) {
		$location = getLocation($bearer, $vId);
		$info = getVehicleInfo($bearer, $vId);
	  
		$vehicle = array();
		$vehicle["vin"] = $vId;
		$vehicle["odometer"] = getOdometer($bearer, $vId);
		$vehicle["latitude"] = $location["latitude"];
		$vehicle["longitude"] = $location["longitude"];
		$vehicle["make"] = $info["make"];
		$vehicle["model"] = $info["model"];
		$vehicle["year"] = $info["year"];

		array_push($vehicles, $vehicle);
	  }

	}
}



$handle = fopen("access_code.txt", "r");
if ($handle) {
	while (($line = fgets($handle)) !== false) {
		$tokens = explode(",", $line);
		getVehicleIds($tokens[0]);
	}
	fclose($handle);
}

$txtFile = "vehicle_data.txt";
$jsonFile = "vehicle_data.json";
$fileOutput = "";

foreach($vehicles as $vehicle) {
	$csv = implode(",", $vehicle);
	$fileOutput .= $csv . "\n";
}

file_put_contents($txtFile, $fileOutput);
file_put_contents($jsonFile, json_encode($vehicles));

?>
</pre>
